# webhooker

A very simple thing for catching webhooks and doing stuff.

The idea here is to provide a simple interface between the webhooks of Github, 
Bitbucket and Gitlab, and the system this runs on. 

It's also to provide a *reasonable* starting point to put together something 
more bespoke; it's generic almost out of spite, simple, well-commented and low
on dependencies (not even `CGI`!).

## Installation

* Set an environment variable pointing to the config file path, and 
`public_html/index.cgi` up as a CGI script (see `./apache.conf` for an example)

* Create the YAML config file (see below or `./webhooker.yaml`)

* Point your webhook at the URL

## Configuring

The config is a YAML file, of the form

    hoster:
      repository:
        branch: 
          directory: /dir/to/cd/to
          command: command-to-run

Where the currently supported 'hosters' are `gitlab`, `bitbucket` and `github`. An example:

    github:
      BigRedS/letsencryptnginx:
        master:
          directory: /home/avi/bin/lengx
          command: git-pull
      github.com/BigRedS/a2:
        master:
          directory: /home/avi/bin/a2/
          command: git-pull
  
    bitbucket:
      BigRedS/example-nginx:
        '*':
          directory: /home/avi/bin
          command: ./update-nginx
    gitlab:
      avi/mywebapp:
        master:
          secret: supersecrettoken
          directory: /home/avi/webapp/
          command: sudo -u avi ./bin/update_codebase.sh
        staging:
          secret: supersecrettoken
          directory: /home/avi/webapp_stg/
          command: sudo -u avi ./bin/update_codebase.sh
      
The 'hoster' is internally-set, and deduced from HTTP headers. The repository 
may be identified either by the `user-name/repo-name` form used above (which 
should be unique across each of Github and Bitbucket) or the SSH clone URL of
the repository (intended to help with self-hosted Gitlab installs). 

The branch may either be an exact match, or an asterisk (which matches all 
branches); there's not (yet) any pattern-matching there.

## webhooker-gitworker

Included is a script called 'gitworker' which is designed to be run by a single
user who is responsible for all git pulls. This is assumed to be 'git-server' 
and is actually hardcoded in some places. That, too, is on the todo list.

This is the more complete way of running Webhoooker. It's expected that you'll 
have a filesystem layout like this:

    git-server@server:~$ find /home/git-server/*
    /home/git-server/apache.conf
    /home/git-server/gitworker
    /home/git-server/logs
    /home/git-server/logs/error_log
    /home/git-server/logs/access_log
    /home/git-server/public_html
    /home/git-server/public_html/index.cgi
    /home/git-server/README.md
    /home/git-server/test
    /home/git-server/webhooker.yaml
    /home/git-server/webhooker.yaml.example

i.e. you've cloned this repo into `~git-server`. You'll use the `apache.conf` 
file to inspire your Apache config and `webhooker.yaml.example` file to inspire 
your webhooker config. 

Ideally, use `mpm-itk` to have the vhost for this run by the git-server user. 
Regardless, whichever user the site is served by will need to be sudoer, and 
able to run the gitworker script as root[0]:

    git-server ALL=(ALL) NOPASSWD: /home/git-server/gitworker

A basic `webhooker.yaml` file might then look like this:

    bitbucket:
      '*':
        '*':
          command: sudo /home/git-server/gitworker "/home/%repo_name%/public_html"

(see below for the variable usage here). that is, every repo is hosted on 
bitbucket, and checked out into a site whose 'owner' is named the same as the 
repo. In this 'single-argument' mode, though, the gitworker is unable to 
actually check-out the repo. To do that it needs to also know the repo URL and 
the branch to checkout, for example:

    bitbucket:
      '*':
        '*':
	  command: sudo /home/git-server/gitworker "/home/%repo_name%/%branch%/public_html" %repo_ssh% %branch%

The generally-preferred way of running this is to have the initial checkout be
run manually as part of the creation of the user and vhost; `webhooker` then is
never asked to do anything potentially destructive.

## Variables in webhooker.yaml commands

Some variables are avialable for substitution into the `command` and/or the 
`directory` values in the `webhooker.yaml` file; they're called by being 
enclosed in percent-symbols (`%branch%`, for example):

Those available are hardcoded into the `interpolate()` sub:

* hoster - the name of the service pushed-to; one of gitlab, bitbucket, or github.
* repo_full_name - the full name of the repo, including the 'owner' For this project,
                   it's either avi/webhooker (on gitlab) or BigRedS/webhooker (on github)
* repo_name - the repo name itself - this project would be 'webhooker'
* repo_http - the http:// or https:// clone URL as provided by the hoster
* repo_ssh - the git:// clone URL as provided by the hoster, or deduced by webhooker
* pusher - the username of the account that actually pushed the change

## Processes

The request is received, the config parsed and the JSON decoded. As soon as 
it's clear that there exists some config for the updated repo/branch another
thread is forked to carry out the configured-actions, and the original process
informs the remote host of the success and exits. That forked process may then
go on to do any work it's configured to do without causing the initial request
to time out waiting for an HTTP response.

## Logging

While still behaving as a CGI script, logs are printed to `STDERR` (so should
end up in the httpd's error log) and to Syslog. On exiting with an error, 
they're also returned as the document body of the error message.

The worker thread still logs to Syslog, but can't speak to the httpd any more.

The CGI script writes logs with an ident of 'webhooker', `gitworker` with one 
of 'webhooker-gitworker'.

## Verifying the sender of the request

If there exists a `secret` element in the config for a branch, then this is 
tested for against the `X-Gitlab-token` header (for gitlab requests only) 
and execution is aborted if they don't match.

Adding GitHub's `sha1`ing is on the todo list.

If you're particularly concerned, you should configure your HTTPd to restrict 
access to only known-good IP addresses here (bitbucket's are in the example 
Apache config).

## Command execution

There's nothing smart going on here; it's a `chdir()` followed by a `system()`.

The bundled `gitworker` script allows for some nominated user (we normally call
it 'git-server' to check out updates for all sites using one ssh keypair.
