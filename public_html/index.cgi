#! /usr/bin/perl

use strict;
use warnings;

use YAML qw/LoadFile/;
use JSON qw/decode_json/;
use Data::Dumper;
use Sys::Syslog qw/openlog syslog/;

my $name='webhooker';
my $version='0.20180630';
my $docs_url='https://github.com/BigRedS/Webhooker';

# So we can write to syslog:
openlog($name, 'pid', 'info');

$SIG{CHLD} = 'IGNORE';
$SIG{INT} = sub{syslog('info', 'Caught SIGINT; exiting'); exit; };

my $f_config = $ENV{WEBHOOKER_CONFIG} || '../webhooker.yaml';
abort("Config file '$f_config' does not exist") unless -f $f_config;
my $config;
eval{ $config = LoadFile($f_config); };
abort($@) if $@;

my @data = <STDIN>;
if(!$data[0] or $data[0] eq ''){
	abort("This is a webhook endpoint, not for browser consumption\n");
}
my $json;
eval{ $json = (decode_json(join('', @data))); };
abort($@) if $@;

if($data[0] =~ m/^\s*{/){
	$json = (decode_json(join('', @data)));
}

# Figure out who sent the data. There's no attempt at *security* here (that's the 
# httpd's job); this is just to work out how best to parse the JSON. 
my $event = {};
my $hoster = undef;
my $hook_id = undef;
if($ENV{'HTTP_USER_AGENT'} and $ENV{'HTTP_USER_AGENT'} =~ m#^Bitbucket-Webhooks#i){
	$event = get_bitbucket_info($json);
	$hoster = 'bitbucket';
	$hook_id = $ENV{'HTTP_X_HOOK_UUID'};
}elsif($ENV{'HTTP_USER_AGENT'} and $ENV{'HTTP_USER_AGENT'} =~ m#^GitHub-Hookshot#i){
	$event = get_github_info($json);
	$hoster = 'github';
	$hook_id = $ENV{'HTTP_X_GITHUB_DELIVERY'};
}elsif($ENV{'HTTP_X_GITLAB_EVENT'} and $ENV{'HTTP_X_GITLAB_EVENT'} =~ m/.+/){
	$event = get_gitlab_info($json);
	$event->{secret} = $ENV{HTTP_X_GITLAB_TOKEN};
	$hoster = 'gitlab';
}else{
	abort("Failed to detect hoster from headers. User Agent: ".$ENV{'HTTP_USER_AGENT'});
	exit;
}

# Having tried to parse it, check that we've actually got something useful out, and
# abort if not.
unless($event->{repo_name}  and $event->{repo_name} =~ m/.+/){
	abort("Failed to parse request '$hook_id' from '$hoster", Dumper($event));
}

# All good, crack on!
syslog('info', "Request '$hook_id' received from '$hoster', pushed by '$event->{pusher}'") if $hook_id and $hoster;

# We want to exit as early as possible, so here we check that all the work we have to
# do (one bit of config per branch) is properly configured, by creating an @todo array
# of all the configs:
my(@todo);
foreach my $branch (@{$event->{branches}}){
	syslog('info', "Processing push to '$branch' on '$event->{repo_full_name}' (at '$event->{hoster}') by '$event->{pusher}'\n");
	print STDERR "Processing push to '$branch' on '$event->{repo_full_name}' (at '$event->{hoster}') by '$event->{pusher}'";

	my $c = get_config_for_commit($event, $config, $branch);
	if($c->{secret} and $c->{secret} =~ m/.+/){
		if($c->{secret} eq $event->{secret}){
			syslog('info', 'Verified secret token');	
		}else{
			syslog('info', "Received secret '$event->{secret}'; expected secret '$c->{secret}'");
			abort("Failed secret-token check");
		}
	}
	push(@todo, $c);
}

# So, now we've validated the sent JSON, we can reply to the sender to let them know
# that everything's okay, and get on with our work without them having to wait:
my $orig_pid = $$;
my($child);
defined($child = fork()) or error("Failed to fork: $!");
if($child){
	syslog('info', 'Worker forked; master process exiting');
	print "Status: 200 OK\n";
	print "X-Powered-By: $name/$version\n\n";
	print "Accepted. PID: $orig_pid\n";
	waitpid($child,0);
}else{
	my $grandchild;
	defined($grandchild = fork()) or error ("Child failed to fork: $!");
	if($grandchild){
		exit;
	}else{
		syslog("info", "Worker $$ forked from $orig_pid");
	
		# Now we do the work!
		foreach my $c (@todo){
			syslog('info', "Working.");
			if($c->{directory}){
				my $directory = interpolate($c->{directory}, $event);
				abort("chdir directory '$directory' does not exist") unless -d $directory;
				syslog('info', "chdir to 'directory'");
				chdir($directory) or abort ("Failed to chdir to '$directory' :$!");
			}
			if($c->{command}){
				syslog('info', "Raw command: $c->{command}");
				my $command = interpolate($c->{command}, $event);
				syslog('info', "Running command: '$command'");
				system($command);
#				my $retval = $? >> 8;
				syslog('info', "Exited: $?\n");
			}
		}
		syslog('info', "Done");
	}
}


#https://apple.stackexchange.com/questions/82438/allow-sudo-to-another-user-without-password

sub get_config_for_commit{
	my $event = shift;
	my $config = shift;
	my $branch = shift;

	my $hoster= $event->{hoster};
	my $repo_full_name = $event->{repo_full_name};

	abort("No config for repos hosted at '$event->{hoster}'") unless($config->{ $event->{hoster} });
	unless($config->{$hoster}->{$repo_full_name}){
		if($config->{$hoster}->{ $event->{repo_ssh} }){
			syslog('info', "Using git SSH URL to identify repo: '$event->{'repo_ssh'}'");
			$repo_full_name = $event->{repo_ssh};
		}elsif($config->{$hoster}->{'*'}){
			syslog('info', "No config for repo '$repo_full_name' at '$hoster'; using wildcard");
			$repo_full_name = '*';
		}else{
			abort("No config for repo '$repo_full_name' hosted on '$hoster'");
		}
	}

	unless($config->{$hoster}->{$repo_full_name}->{$branch}){
		if($config->{$hoster}->{$repo_full_name}->{'*'}){
			syslog('info', "No config for repo branch '$branch' of '$repo_full_name' at '$hoster'; using wildcard");
			$branch = '*';
		}else{
			abort("No config for branch '$branch' of '$repo_full_name' at '$hoster'");
		}
	}
	syslog('info', "Detected repo: hoster: $hoster, Repo: $repo_full_name, Branch: $branch");
	return $config->{$hoster}->{$repo_full_name}->{$branch};
}

# Called when something's gone wrong and we're going to exit. Passed an error
# message, logs it to syslog, the httpd's error log (by writing it to stdout) 
# and writes it in the response-body (having set a 400 status code).
sub abort{
	my $message = shift;
	my @extra_messages = @_;
	syslog('info', "Aborting: $message");
	print STDERR "($name) Aborting: $message\n";
	print "Status: 400 Bad Request\n";
	print "X-Powered-By: $name/$version\n\n";
	print "$name: $message\n";
#	print "See $docs_url for documentation\n";
	print "\n".join("\n", @extra_messages);
	exit;
}

sub interpolate{
	my $string = shift;
	my $event = shift;
	my $branchname = shift;

	foreach my $key (qw/hoster repo_full_name repo_name repo_http repo_ssh pusher/){
		$string =~ s/\%$key\%/$event->{$key}/g;
	}
	$string =~ s/\%branch\%/$branchname/g;
	return $string;
}


# One sub per 'hoster'; we have a standard-shaped hash, and these functions 
# are given the data parsed from the JSON, and return it in that shape:
sub get_github_info{
	my $data = shift;
	my $info = {};
	$info->{hoster} = 'github';
	$info->{repo_full_name} = $data->{repository}->{full_name};
	$info->{repo_name} = $data->{repository}->{name};
	$info->{repo_http} = $data->{repository}->{clone_url};
	$info->{repo_ssh} = $data->{repository}->{ssh_url};
	$info->{pusher} = $data->{pusher}->{name};
	my $branch = (split(m#/#, $data->{ref}))[2];
	push(@{$info->{branches}}, $branch);
	return $info;
}

sub get_bitbucket_info{
	my $data = shift;
	my $info = {};
	$info->{hoster} = 'bitbucket';
	$info->{repo_name} = $data->{repository}->{name};
	$info->{repo_full_name} = $data->{repository}->{full_name};
	$info->{repo_http} = $data->{repository}->{links}->{html}->{href};
	$info->{repo_ssh} = $info->{repo_http};
	$info->{repo_ssh} =~ s#^https://bitbucket.org/#git\@bitbucket.org:#;
	$info->{pusher} = $data->{actor}->{username};
	my %branches;
	foreach my $change (@{$data->{push}->{changes}}){
		$branches{ $change->{new}->{name} }++;
	}
	@{$info->{branches}} = (keys(%branches));
	return $info;
}

sub get_gitlab_info{
	my $data = shift;
	my $info = {};
	$info->{hoster} = 'gitlab';
	$info->{repo_name} = $data->{project}->{name};
	$info->{repo_full_name} = $data->{project}->{path_with_namespace};
	$info->{repo_http} = $data->{project}->{http_url};
	$info->{repo_ssh} = $data->{project}->{ssh_url};
	$info->{pusher} = $data->{user_username};
	my $branch = (split(m#/#, $data->{ref}))[2];
	push(@{$info->{branches}}, $branch);
	return $info;
}

sub validate_info{
	my $info = shift;
	abort("Illegal repo_name") unless $info->{repo_name} =~ m#^[\w\d\.-_]+$#;
	abort("Illegal repo_full_name") unless $info->{repo_full_name} =~ m#^[\w\d\.-_]+/$info->{repo_name}#;
}

exit;
